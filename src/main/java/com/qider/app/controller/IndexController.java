package com.qider.app.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Document.OutputSettings.Syntax;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itextpdf.text.DocumentException;
import com.qider.app.util.PDFGenerator;
import cn.hutool.core.lang.UUID;

@Controller
@RequestMapping("index")
public class IndexController {

    @RequestMapping("/index")
    public String index(HttpServletRequest request) {
        return "view/index";
    }

    @RequestMapping(value = "push", produces = "text/event-stream")
    public @ResponseBody String push() {
        System.out.println("push msg..");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 注意：返回数据的格式要严格按照这样写，‘\n\n’不可少
        return "data:current time: " + new SimpleDateFormat("YYYY-MM-dd hh:mm:ss").format(new Date()) + "\n\n";
    }

    @RequestMapping(value = "/test")
    @ResponseBody
    public Map<String, Object> test(HttpServletRequest request, HttpServletResponse response) {
        return null;
    }

    @RequestMapping("/echart")
    public String echart(HttpServletRequest request) {
        return "view/echart";
    }

    @RequestMapping(value = "/view")
    @ResponseBody
    public Map<String, Object> view(HttpServletRequest request, HttpServletResponse response) {

        Map<String, Object> result = new HashMap<>(4);

        System.setProperty("webdriver.firefox.bin", "D:\\Program Files\\Mozilla Firefox\\firefox.exe");
        System.setProperty("webdriver.gecko.driver", "D:\\Program Files\\Mozilla Firefox\\geckodriver.exe");
        String path = request.getContextPath();
        String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/index/echart";

        FirefoxOptions option = new FirefoxOptions();
        option.setHeadless(true); // 无界面浏览器

        WebDriver webDriver = new FirefoxDriver(option);
        webDriver.manage().deleteAllCookies(); // 删除cookies
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        webDriver.get(url); // 打开echarts页面
        String res = webDriver.getPageSource();
        Document html = Jsoup.parse(res);
        html.outputSettings().syntax(Syntax.xml); // 用jsoup格式化html代码，以防止标签没有关闭导致的转换报错。
        String filePath = "d:/" + UUID.randomUUID() + ".pdf";
        try {
            PDFGenerator.topdf(html.toString(), filePath);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            webDriver.close();
        }
        result.put("path", filePath);

        return result;
    }

}
