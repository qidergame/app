/**
 * TODO
 * 
 * @package com.qider.app.dao
 * @file SysUserDao.java
 * @author zhaokuidong
 * @Date 2020年4月10日 下午2:12:36
 * @version V 1.0
 */
package com.qider.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.qider.app.entity.Org;

/**
 * TODO
 * 
 * @package com.qider.app.dao
 * @file SysUserDao.java
 * @author zhaokuidong
 * @date 2020年4月10日 下午2:12:36
 * @version V 1.0
 */
@Repository
public interface OrgDao extends JpaRepository<Org, String> {

}
