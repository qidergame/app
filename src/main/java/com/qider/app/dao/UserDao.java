/**
 * TODO
 * 
 * @package com.qider.app.dao
 * @file SysUserDao.java
 * @author zhaokuidong
 * @Date 2020年4月10日 下午2:12:36
 * @version V 1.0
 */
package com.qider.app.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.qider.app.entity.User;

/**
 * TODO
 * @package com.qider.app.dao
 * @file SysUserDao.java
 * @author zhaokuidong
 * @date 2020年4月10日 下午2:12:36
 * @version V 1.0
 */
@Repository
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	<T> Collection<T> findByUsername(String username, Class<T> type);
}
