package com.qider.app.dao;

import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;
import com.qider.app.entity.User;

@Repository
public interface UserEnversRevisionRepository extends RevisionRepository<User, String, Integer>{

}
