package com.qider.app.projection;

public interface UserProjection {

	String getId();

	String getUsername();

	Integer getAge();
}
