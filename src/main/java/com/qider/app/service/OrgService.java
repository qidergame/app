package com.qider.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.qider.app.dao.OrgDao;
import com.qider.app.entity.Org;

@Service
public class OrgService {

    @Autowired
    private OrgDao orgDao;

    public Org save(Org org) {
        return orgDao.save(org);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Org findById(String id) {
        return orgDao.findById(id).orElse(null);
    }

}
