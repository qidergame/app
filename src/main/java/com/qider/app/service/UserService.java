package com.qider.app.service;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.qider.app.dao.UserDao;
import com.qider.app.dao.UserEnversRevisionRepository;
import com.qider.app.entity.User;
import com.qider.app.projection.UserProjection;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService {

	@Autowired
	UserDao userDao;

	@Autowired
	UserEnversRevisionRepository userEnversRevisionRepository;

	@Transactional(propagation = Propagation.REQUIRED)
	@Cacheable(value = "user", key = "targetClass.simpleName + ':' + methodName + ':' + #p0")
	public User findById(String id) {
		return userDao.findById(id).get();
	}

	public User save(User user) {
		return userDao.save(user);
	}

	public User getById(String id) {
		return userEnversRevisionRepository.findLastChangeRevision(id).get().getEntity();
	}

	public List<User> saveAll(List<User> users) {
		return userDao.saveAll(users);
	}

	public List<User> findAll() {
		return userDao.findAll();
	}

	public boolean deal() {
		log.info("{}-{}-{}-{}", this.getClass().getSimpleName(), "deal", LocalDateTime.now());
		return true;
	}

	public Collection<UserProjection> findByUsername(String username) {
		return userDao.findByUsername(username, UserProjection.class);
	}

	public Collection<UserProjection> findByName(String username) {
		Specification<User> spec = new Specification<User>() {

			private static final long serialVersionUID = 1L;

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("username"), username);
			}
		};

		return userDao.findAll(spec, UserProjection.class);
	}
}
