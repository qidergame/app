package com.qider.app;

import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qider.app.projection.UserProjection;
import com.qider.app.service.OrgService;
import com.qider.app.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest(classes = Application.class)
public class ApplicationTests {

	@Autowired
	private UserService userService;

	@SuppressWarnings("unused")
	@Autowired
	private OrgService orgService;

	@Test
	public void test2() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		Collection<UserProjection> list = userService.findByName("aa9");

		log.info("collection={}", list);
		System.out.println(objectMapper.writeValueAsString(list));

	}

}
