package com.qider.app;

import com.qider.app.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class Person extends BaseEntity {

    /**
     * serialVersionUID : TODO
     */
    private static final long serialVersionUID = -5394945829977553731L;
    private String name;
}
